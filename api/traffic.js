
import axios from 'axios'

export default {
  getTraffic(params) {
    let request = axios.get('/traffic', { params });
    return request
      .then(result => { return result; })
      .catch(error => { console.error(error); return Promise.reject(error); });
  },
  uploadCsv(formData) {
    let request = axios.post('/traffic', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    });
    return request
      .then(result => { return result; })
      .catch(error => { console.error(error); return Promise.reject(error); });
  },
  deleteRecord(item) {
    let request = axios.delete('/traffic/' + item.id);
    return request
      .then(result => { return result; })
      .catch(error => { console.error(error); return Promise.reject(error); });
  },
  updateRecord(item) {
    let request = axios.put('/traffic/' + item.id, item);
    return request
      .then(result => { return result; })
      .catch(error => { console.error(error); return Promise.reject(error); });
  }
}
