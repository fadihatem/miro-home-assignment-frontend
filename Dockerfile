
# build stage
FROM node:14.15.5-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
WORKDIR /etc/nginx/conf.d
RUN sed -n -i 'p;10a try_files $uri $uri/ /index.html;' default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]