import api from '../../api/traffic'

// initial state
// shape: [{ id, quantity }]
const state = () => ({
  traffic: [],
  totalTraffic: 0,
  totalTrafficPages: 1,
  loading: false,
  editItem: null,
  newGoalValue: null,
  error: {
    show: false,
    type: "error",
    message: "Notification Message"
  }
})

// getters
const getters = {
  getLoading: (state) => state.loading,
  errorShow: (state) => state.error.show,
  errorType: (state) => state.error.type,
  errorMessage: (state) => state.error.message,
}

// actions
const actions = {
  getTraffic({ commit }, payload) {
      return api.getTraffic(payload).then((data) => {
        commit('setTraffic', data);
      })
  },
  uploadCsv({ commit }, payload) {
    return api.uploadCsv(payload.content).then((data) => {
      this.dispatch('traffic/getTraffic');
    })
  },
  deleteRecord({ commit }, payload) {
    return api.deleteRecord(payload).then((data) => {
      commit('deleteRecord', payload)
    })
  },
  updateGoal({ commit , state },payload){
    payload['id'] = state.editItem.id;
    return api.updateRecord(payload).then((response) => {
      commit('updateGoal',response.data.data)
    })
  },
  editRow({ commit }, payload) {
    commit('editRow', payload)
  },
  toggleLoader({ commit }) {
    commit('toggleLoader')
  },
  closeEditDialog({ commit }) {
    commit('closeEditDialog')
  },
  errorResponce({commit}, payload) {
    commit('showError', payload)
  }
}

// mutations
const mutations = {
  toggleLoader(state) {
    state.loading = !state.loading
  },
  setTraffic(state, traffic) {
    state.traffic = traffic.data.data.data
    state.totalTraffic = traffic.data.data.total
    state.totalTrafficPages = traffic.data.data.last_page
  },
  deleteRecord(state, item) {
    state.traffic.splice(state.traffic.indexOf(item), 1)
  },
  updateGoal(state, item){
    state.traffic[state.traffic.indexOf(state.traffic.find(row => row.id === item.id))] = item
  },
  closeEditDialog(state){
    state.editItem = null
  },
  editRow(state, item){
    state.editItem = item
    state.newGoalValue = item.goal_value
  },
  showError(state,payload){
    state.error = payload
    setTimeout(()=>{
      state.error.show = false
    }, 4000);
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
