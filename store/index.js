import Vue from 'vue'
import Vuex from 'vuex'
import traffic from './modules/traffic'
import createLogger from '../src/plugins/logger'
import axios from 'axios'

Vue.use(Vuex)

axios.defaults.baseURL = 'http://localhost:8001/api'

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    traffic,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
